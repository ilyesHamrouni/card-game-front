# CardGame

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Installation and Requirements

-------------------
- clone the project 
- npm install 
- ng serve


-----------------
## Design and architecture

- Game component: responsible for displaying and sorting the cards
- Rules component: responsible for displaying the rules of the game 
- Main component : the home page of the application
- Router: to route to different compoennts

# Best Practices Git
 - Git : a main branch for the release and branch for the developement (dev)

