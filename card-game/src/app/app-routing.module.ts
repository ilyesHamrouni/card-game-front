import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game/game.component';
import { MainComponent } from './main/main.component';
import { RulesComponent } from './rules/rules.component';



const routes: Routes = [
  {path: "",
  
  component: MainComponent},
  {
    path:"game",
    component: GameComponent
  },
  {
    path:"rules",
    component:RulesComponent
  }


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}
