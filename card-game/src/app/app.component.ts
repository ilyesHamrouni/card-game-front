import { Component, OnInit } from '@angular/core';
import { Card } from './models/Card.model';
import { SortCriteria } from './models/SortCriteria.model';
import { CardService } from './services/card.service';
import { SUITE } from "./models/CardConstants";
import { Hand } from './models/Hand.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'card-game';

  
}
