import { Component, OnInit } from '@angular/core';
import { Card } from '../models/Card.model';
import { Hand } from '../models/Hand.model';
import { SortCriteria } from '../models/SortCriteria.model';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {




  private hand:Hand;
  public selectedCardTitle:string ='Generated Hand';
  public cardTypeSortTitle:string ='Sort Order For Card Suite';
  public cardValueSortTitle:string = 'Sort Order For Card Rank';
  public sortedCardTittle:string ='Sorted Hand';
  


  public sortedCards:Card[];
  public selectedCards:Card[];
  public cardTypeSort:Card[];
  public cardValueSort:Card[];

  public isSortButtonDisabled:boolean =true;
  constructor(private cardService:CardService){


  }

  ngOnInit(){
    //test data 

  }

  private reset():void{
    this.selectedCards=[];
    this.cardTypeSort=[];
    this.cardValueSort=[];
    this.isSortButtonDisabled=true;
  }

  public sortHand(){
    this.cardService.sortHand(this.hand).subscribe(
      (sortedHand:Hand)=>{
        console.log("sorted hand",sortedHand);
        sortedHand.hand.forEach(card => card.icon=this.getIconName(card.suite,card.rank));     
        this.sortedCards= [...sortedHand.hand];
      }
    )
  }

  public generateGame(){
    this.cardService.getHand(10).subscribe(
      (hand:Hand)=>{
        this.reset();
        console.log("hand:",hand);
        this.hand= hand;
        this.setSelectedCards(hand.hand);
        this.buildSortCriteria(hand.sortCriteria);
      }
    )
  }

  public getHandTest(){
    let hand = this.cardService.getHandTest(10);
    console.log("hand:",hand);
    this.setSelectedCards(hand.hand);
    this.buildSortCriteria(hand.sortCriteria);
  }

  private setSelectedCards(cards:Card[]):void{
    cards.forEach( card=>   {card.icon= this.getIconName(card.suite,card.rank)} );
    this.selectedCards=[...cards];
    this.isSortButtonDisabled=false;
    console.log("selected cards:",this.selectedCards);
  }

  private buildSortCriteria(sortCriteria:SortCriteria):void{
    sortCriteria.suiteSortCriteria.forEach(type=>this.cardTypeSort.push(this.getExamplaryCardBySuite(type)));
    sortCriteria.rankSortCriteria.forEach(value=>this.cardValueSort.push(this.getExamplaryCardByRank(value)));
    console.log("card suite sort:",this.cardTypeSort);
    console.log("card rank sort:",this.cardValueSort);
  }

  private getExamplaryCardBySuite(suite:string):Card{
    let card:Card ={
      suite:suite,
      rank:'ace',
      icon:this.getIconName(suite,'ace')

    }
    return card;
  }
  private getExamplaryCardByRank(value:string):Card{
    let card:Card={
      suite:'diamonds',
      rank:value,
      icon:this.getIconName('diamonds',value)
    }
    return card;
  }


  private getIconName(suit:string,rank:string):string{

    return this.mapRankToNumerical(rank)+"_of_"+ suit.toLowerCase()+".svg";
  }

  private mapRankToNumerical(rank:string):string{
    switch(rank){
      case 'two':
        return '2';
      case 'three':
        return '3';
      case 'four':
        return '4';
      case 'five':
        return '5';
      case 'six':
        return '6';
      case 'seven':
        return '7';
      case 'eight':
        return '8';
      case 'nine':
        return '9';
      case 'ten':
        return '10';
      default:
        return rank;
    }

  }

}
