export interface Card{
    suite:string;
    rank:string;
    icon?:string;
}