export const HEARTS = 'hearts';
export const DIAMONDS = 'diamonds';
export const SPADES = 'spades';
export const CLUBS ='clubs';

export const SUITE =[
    HEARTS,
    DIAMONDS,
    SPADES,
    CLUBS
]

