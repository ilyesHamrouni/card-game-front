import { Card } from "./Card.model";
import { SortCriteria } from "./SortCriteria.model";

export interface Hand{
    hand: Card[]; 
    sortCriteria: SortCriteria;
}