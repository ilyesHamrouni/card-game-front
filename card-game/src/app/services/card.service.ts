import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Hand } from '../models/Hand.model';


const URL= "http://localhost:8080/v1/hand" ;
@Injectable({
  providedIn: 'root'
})
export class CardService {

  

  constructor(private http:HttpClient) { }

  public getHand(numberOfCards:number):Observable<Hand> {
    return this.http.get<Hand>(URL+"/generate-random-hand/"+numberOfCards);

  }
  public sortHand(hand:Hand):Observable<Hand>{
    return this.http.post<Hand>(URL+"/sort-hand",hand);
  }

  public getHandTest(numberOfCards){
    let observeable = new Observable();
      let hand:Hand ={
        hand:[
          {
            suite:'diamonds',
            rank:'5',
            
          },
          {
            suite:'spades',
            rank:'ace',
            
          },
          {
            suite:'clubs',
            rank:'10',
            
          },
          {
            suite:'hearts',
            rank:'jack',
            
          },
          {
            suite:'spades',
            rank:'7',
            
          },
        ],
        sortCriteria:{
          suiteSortCriteria:['spades','diamonds','hearts','clubs'],
          rankSortCriteria:['2','3','4','5','6','7','8','9','10','jack','queen','king','ace']
        }
      }

      return hand;

  }
}
